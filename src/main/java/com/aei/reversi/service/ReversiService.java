package com.aei.reversi.service;

import com.aei.reversi.domain.Board;
import com.aei.reversi.domain.Coordinate;
import com.aei.reversi.domain.Disc;
import com.aei.reversi.domain.Player;

import java.util.*;

public class ReversiService {

    private static ReversiService reversiService;

    private ReversiService() {}

    public static synchronized ReversiService getInstance() {
        if (reversiService == null) {
            reversiService = new ReversiService();
        }
        return reversiService;
    }

    private Board board;
    private Set<Player> players = new HashSet<>(2);
    private Player current = null;
    private UserInputs userInputs;

    public Player start(Integer width, Integer height, List<Integer> darkIndices, List<Integer> lightIndices, UserInputs userInputs) {
        this.userInputs = userInputs;
        board = new Board(width, height, darkIndices, lightIndices);
        Player playerX = new Player(Disc.DARK, board, userInputs);
        Player playerO = new Player(Disc.LIGHT, board, userInputs);
        players.add(playerX);
        players.add(playerO);
        current = playerX;
        board.initialize();

        loop();
        print();
        Player winner = sumUp();
        reversiService = null;
        return winner;
    }

    private void print() {
        List<Coordinate> matrix = board.getMatrix();
        Integer width = board.getWidth();
        List<Character> alphabets = board.getAlphabets();

        for (int i = 0; i < matrix.size(); i++) {
            String content = String.join("", String.valueOf(matrix.get(i)));
            System.out.print((i % width == 0 ? "\n" + ((i / width) + 1) + " ": "") + content);
        }
        for (int i = 0; i < width; i++) {
            System.out.print(((i == 0) ? "\n  " : "") + alphabets.get(i) + ((i == (width - 1)) ? "\n" : ""));
        }
    }

    private void loop() {
        print();

        System.out.print("\nPlayer " + current + " move: ");
        String input = userInputs.retrievePlayerInput();
        current.move(input);

        Player next = players.stream()
                .filter(player -> !player.equals(current))
                .findFirst()
                .orElse(current);
        current = next;
        if (!board.isAllDiscsInOneSide() && !board.isFull()) {
            loop();
        }
    }

    private Player sumUp() {
        List<Coordinate> matrix = board.getMatrix();
        Long lightCount = matrix.stream()
                .filter(coordinate -> Disc.LIGHT.equals(coordinate.getDisc()))
                .count();
        Long darkCount = matrix.stream()
                .filter(coordinate -> Disc.DARK.equals(coordinate.getDisc()))
                .count();
        Boolean isDarkWin = lightCount < darkCount;
        Disc winDisc = Disc.LIGHT;
        List<String> score = Arrays.asList(lightCount.toString(), "vs", darkCount.toString());
        if (isDarkWin) {
            Collections.reverse(score);
            winDisc = Disc.DARK;
        }
        Disc finalWinDisc = winDisc;
        Player winner = players.stream()
                .filter(player -> finalWinDisc.equals(player.getDisc()))
                .findFirst()
                .orElse(current);

        System.out.println("No further moves available\n" +
                "Player " + winner + " wins ( " + String.join(" ", score) + " )");
        return winner;
    }
}
